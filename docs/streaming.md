# Streaming API

Elstat's Streaming API enables clients to receive realtime
updates from the server.

Clients can optionally subscribe to certain channels using the
Streaming API if they don't want to have a lot of overhead.

## Common payload format

Payloads **sent from the client** can not exceed 512 bytes in size. Exceeding
this limit will cause the websocket to close with the `TOO_MUCH` error code.

Payloads are JSON encoded.

### Structure

```javascript
{
    op: int,
    channels?: [string]
}
```

Data in packets are stored in specific fields, which are described
for each OP below. They are not *always* stored inside a `d` field,
in contrast to Discord's Gateway API.


## Error codes

| Error code | Name |
|---:|:---|
|4200|`INVALID_PAYLOAD`|
|4420|`TOO_MUCH`|


## OP Codes

| OP Int | OP Name | Sent/Received by client |
|--:|:--|:--:|
|-1|`UNSUBSCRIBE`|Sent|
|0|`SUBSCRIBE`|Sent|
|1|`SUBSCRIBED`|Received|
|2|`UNSUBSCRIBED`|Received|
|3|`DATA`|Received|
|4|`INCIDENT_NEW`|Received|
|5|`INCIDENT_UPDATE`|Received|
|6|`INCIDENT_CLOSE`|Received|

## OP Payloads

 - `SUBSCRIBE`
    - field: `channels` (`list[str]`), array of channels you want to subscribe to.
    - reply: `SUBSCRIBED` payload.
 - `UNSUBSCRIBE`
    - field: `channels` (`list[str]`), array of channels you want to **un**subscribe from.
    - reply: `UNSUBSCRIBED` payload.
 - `SUBSCRIBED`
    - field: `channels` (`list[str]`), array of channels you *succesfully* subscribed to.
 - `UNSUBSCRIBED`
    - field: `channels` (`list[str]`), array of channels you *succesfully* **un**subscribed from.
 - `DATA`
    - field: `c` (`str`), the channel the data is coming from.
    - data: `d` (`list[any]`), the data coming in from the channel.
      - The first element of `d` is an integer, encoding an UNIX timestamp with millisecond precision.
      - The second element of `d` is described on *`Channel types`*

 - `INCIDENT_NEW`
   - data: `d` (`dict`), an incident payload
 - `INCIDENT_UPDATE`
   - data: `d` (`dict`), an incident payload.
 - `INCIDENT_CLOSE`
   - data: `d` (`dict`), an incident payload, containing at least the `id` field.

Incident payloads are described on the Incidents API documentation.


## Channel names

Clients subscribe to channels to receive specific data. Channel names
are formatted like: `<type>:<name>`

e.g `status:elixire` and `graph:elixire`

**There is a special channel for incident reporting
(`INCIDENT_\*` OP codes): `incidents`**


### Channel types
 - `status` channel
    - Returns a boolean, representing the status of the service
 - `latency` channel
    - Returns an integer, representing the latency of the service,
        in *milliseconds*.


## Connection logic

 - Connect a websocket to `/api/streaming`.
 - Send a `SUBSCRIBE` payload, receive `SUBSCRIBED` back
 - Listen to `DATA` payloads and update local state as needed.
 - Optionally: listen to `INCIDENT_{NEW|UPDATE|CLOSE}`
