# Elstat API Docs

The documentation here is for the part of the API that shows a client
information about the services being checked by the instance.

## `GET /api/status`

### Output

```javascript
// the value string represents the uptime of the service
// and is a string because high precision is required.
uptime_t = {[string]: string}

// the first integer is a unix timestamp
// the second integer represents latency in milliseconds (can be float)
graph_t = [ [integer, number], ... ]

// main information about a service.
service_data = {
    description: string
    status: boolean,

    // if something happened to the service, clients
    // should signal about this (if possible)
    error?: string,

    latency?: number,
    timestamp?: integer,
}

// now this is the output for /api/status.
{
    // number of milliseconds before considering a service
    // slow in its latency.
    slow_threshold: integer,

    status: {[string]: service_data},
    graph: {[string]: graph_t},

    // daily uptime, weekly uptime and monthly uptime
    // respectively.
    uptime: uptime_t,
    week_uptime: uptime_t,
    month_uptime: uptime_t,
}
```

## `GET /api/quick`

Quicker version of `GET /api/status`, without the `graph` field. Suitable
for scripts.
