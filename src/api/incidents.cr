require "kemal"

require "../helpers"
require "../config_checker"
require "../auth"

require "./streaming"

module IncidentType
  Outage        = "outage"
  PartialOutage = "partial_outage"
  Degraded      = "degraded_service"
end

VALID_INCIDENT_TYPES = ["outage", "partial_outage", "degraded_service"]

enum IncidentAlertType
  Create
  Close
  NewStage
end

class IncidentStage
  property timestamp : Int64
  property title : String
  property content : String

  def initialize(@timestamp, @title, @content)
  end

  def to_map
    {timestamp: @timestamp, title: @title, content: @content}
  end
end

class Incident
  property id : UUID
  property incident_type : String
  property title : String
  property content : String
  property ongoing : Bool

  property start_ts : Int64
  property stages : Array(IncidentStage) = Array(IncidentStage).new
  property end_ts : (Int64 | Nil)

  def initialize(id : String, @incident_type, @title, @content, @ongoing, @start_ts, @end_ts = nil)
    @id = UUID.new(id)
  end

  def to_map
    stages_as_maps = @stages.map do |stage|
      stage.to_map
    end

    {
      id:              @id.to_s,
      incident_type:   @incident_type,
      title:           @title,
      content:         @content,
      ongoing:         @ongoing,
      start_timestamp: @start_ts,
      end_timestamp:   @end_ts,
      stages:          stages_as_maps,
    }
  end

  def to_json
    to_map.to_json
  end
end

get "/api/incidents/current" do |env|
  manager = get_manager env

  incident_id = manager.@context.db.query_one?(
    "select id
    from incidents
    where ongoing = 1
    order by id asc
    limit 1", as: {String})

  if incident_id.nil?
    "null"
  else
    incident = manager.fetch_incident(UUID.new(incident_id))
    incident.to_json
  end
end

get "/api/incidents/:page" do |env|
  manager = get_manager env
  page = env.params.url["page"].to_i64

  rows = manager.@context.db.query_all(
    "select id
    from incidents
    order by start_timestamp desc
    limit 10
    offset (#{page} * 10)",
    as: {String}
  )

  incidents = [] of Incident

  rows.each do |row|
    incident = manager.fetch_incident(UUID.new(row))
    if !incident.nil?
      incidents.push(incident)
    end
  end

  res = incidents.map do |incident|
    incident.to_map
  end

  res.to_json
end

private def ensure_incident_valid(inc_type_json)
  inc_type = inc_type_json.as(String)

  if !VALID_INCIDENT_TYPES.includes?(inc_type)
    raise "invalid incident type"
  end
end

post "/api/incidents" do |env|
  do_login
  manager = get_manager env

  incident_id = UUID.random
  start_ts = Time.utc.to_unix_ms
  j = env.params.json

  ensure_incident_valid j["type"]

  manager.@context.db.exec(
    "insert into incidents
      (id, incident_type, title, content,
       ongoing, start_timestamp, end_timestamp)
    values (?, ?, ?, ?, 1, ?, NULL)",
    incident_id.to_s,
    j["type"].as(String),
    j["title"].as(String),
    j["content"].as(String),
    start_ts)

  incident = manager.fetch_incident(incident_id)

  if incident.nil?
    raise "assert failure: incident is nil"
  end

  manager.dispatch_incident(MessageOP::IncidentNew, incident)
  manager.alert_incident(IncidentAlertType::Create, incident_id)

  incident.to_json
end

private def get(j, incident, field : String)
  value = j[field]?

  if value.nil?
    return incident.to_map[field]
  end

  return value
end

patch "/api/incidents/:incident_id" do |env|
  do_login
  incident_id = UUID.new(env.params.url["incident_id"])
  j = env.params.json

  manager = get_manager env
  db = manager.@context.db

  incident = manager.fetch_incident incident_id
  if incident.nil?
    halt env, 404
  end

  if !j.has_key?("end_date") && !j["ongoing"]
    j["end_date"] = Time.utc.to_unix_ms
  end

  ensure_incident_valid j["type"]

  db.exec(
    "update incidents
    set incident_type = ?, title = ?, content = ?, ongoing = ?,
      start_timestamp = ?, end_timestamp = ?
    where id = ?",
    get(j, incident, "type"),
    get(j, incident, "title"),
    get(j, incident, "content"),
    get(j, incident, "ongoing"),
    get(j, incident, "start_date"),
    get(j, incident, "end_date"),
    incident_id.to_s
  )

  incident = manager.fetch_incident incident_id
  if incident.nil?
    raise "assert fail incident post-update not found"
  end

  if j["ongoing"]?
    manager.dispatch_incident(MessageOP::IncidentUpdate, incident)
  else
    manager.dispatch_incident(MessageOP::IncidentClose, incident)
    manager.alert_incident(IncidentAlertType::Close, incident_id)
  end
end

post "/api/incidents/:incident_id/stages" do |env|
  do_login
  incident_id = UUID.new(env.params.url["incident_id"])
  j = env.params.json

  manager = get_manager env
  db = manager.@context.db

  stage_ts = Time.utc.to_unix_ms

  db.exec(
    "insert into incident_stages (parent_id, timestamp, title, content)
    values (?, ?, ?, ?)",
    incident_id.to_s, stage_ts, j["title"], j["content"])

  incident = manager.fetch_incident incident_id
  if incident.nil?
    raise "Unknown incident"
  end

  manager.dispatch_incident(MessageOP::IncidentUpdate, incident)
  manager.alert_incident(IncidentAlertType::NewStage, incident_id)

  stage = incident.stages.last(1)[0]
  stage.to_map.merge({
    parent_id: incident_id.to_s,
  }).to_json
end
