require "kemal"
require "logger"
require "time"
require "big"

require "../helpers"
require "../adapters"

def get_status(manager)
  res = {} of String => Hash(String, AdapterValue | UInt64)

  manager.service_names.each do |serv_name|
    state = manager.@state[serv_name]
    thresholds = manager.@context.thresholds serv_name
    res[serv_name] = {
      "timestamp"         => state.timestamp,
      "status"            => state.status,
      "latency"           => state.latency,
      "error"             => state.error,
      "description"       => manager.@context.cfg["service:#{serv_name}"]["description"],
      "down_threshold"    => thresholds[:down],
      "slow_threshold"    => thresholds[:slow],
      "latency_threshold" => thresholds[:latency],
    }
  end

  return res
end

def get_cache(manager, name, period)
  if period.is_a?(Time::Span)
    manager.uptime_cache[{name, period}]?
  elsif period.is_a?(Time::MonthSpan)
    manager.month_uptime_cache[{name, period}]?
  end
end

def put_cache(manager, name, period, uptime)
  if period.is_a?(Time::Span)
    manager.uptime_cache[{name, period}] = uptime.to_s
  elsif period.is_a?(Time::MonthSpan)
    manager.month_uptime_cache[{name, period}] = uptime.to_s
  end
end

def finish_timing(start_ts : Time::Span) : UInt64
  end_ts = Time.monotonic
  total = end_ts - start_ts
  return total.total_microseconds.to_u64
end

def raw_uptime(manager, name : String, period : (Time::Span | Time::MonthSpan)) : Tuple(UptimeData, UInt64)
  # check if cache has it, and if not, calculate it with the database
  start_ts = Time.monotonic
  uptime_value = get_cache(manager, name, period)

  if !uptime_value.nil?
    return uptime_value, finish_timing(start_ts)
  end

  max_tstamp = (Time.utc - Time::UNIX_EPOCH).total_milliseconds
  min_tstamp = ((Time.utc - period) - Time::UNIX_EPOCH).total_milliseconds

  period_ms = max_tstamp - min_tstamp

  db = manager.@context.db
  cfg = manager.@context.cfg

  down_hits = db.scalar(
    "select count(*) from #{name}
    where status = 0 and timestamp > ?",
    min_tstamp).as(Int64)

  # calculate downtime in milliseconds and then convert it to a
  # percentage from the total period_ms
  downtime = down_hits * cfg["service:#{name}"]["poll"].to_u64 * 1000
  downtime = BigFloat.new(downtime)
  downtime = downtime / BigFloat.new(period_ms)

  one_hundred = BigFloat.new(100)
  uptime = one_hundred - downtime

  # cache the calculated value
  put_cache(manager, name, period, uptime)
  return uptime.to_s, finish_timing(start_ts)
end

def get_uptime(manager, period : (Time::Span | Time::MonthSpan))
  res = {} of String => UptimeData
  timings = {} of String => UInt64

  manager.service_keys.each do |serv_key|
    name = serv_key.lchop("service:")
    uptime, timing = raw_uptime(manager, name, period)

    res[name] = uptime
    timings[name] = timing
  end

  return res, timings
end

def quick_response(manager)
  day_uptime, day_uptime_timing = get_uptime(manager, 1.day)
  week_uptime, week_uptime_timing = get_uptime(manager, 1.week)
  month_uptime, month_uptime_timing = get_uptime(manager, 1.month)

  return {
    status:       get_status(manager),
    uptime:       day_uptime,
    week_uptime:  week_uptime,
    month_uptime: month_uptime,

    uptime_timing: {
      day:   day_uptime_timing,
      week:  week_uptime_timing,
      month: month_uptime_timing,
    },
  }
end

get "/api/quick" do |env|
  manager = get_manager env
  quick_response(manager).to_json
end

get "/api/status" do |env|
  manager = get_manager env
  quick_response(manager).merge({graph: manager.fetch_all_graphs}).to_json
end
