require "kemal"
require "logger"
require "signal"

require "./context"
require "./manager"
require "./config_checker"

require "./api/main"
require "./api/streaming"
require "./api/incidents"

ctx = Context.new
check_config(ctx.cfg)
ctx.setup

manager = Manager.new(ctx)
manager.setup

add_context_storage_type(Manager)
add_context_storage_type(Logger)

before_all "*" do |env|
  env.set "manager", manager
  env.set "log", manager.context.log
  env.response.content_type = "application/json"

  cors_config = manager.context.cfg["elstat:cors"]?
  unless cors_config.nil?
    available_origins = cors_config["origins"]?
    unless available_origins.nil?
      env.response.headers.add("Access-Control-Allow-Origin", available_origins)
    end
  end

  env.response.headers.add("Access-Control-Allow-Headers", "Authorization")
  env.response.headers.add("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE")
end

get "/awoo" do
  raise "uwu"
end

error 500 do |env, err|
  {error:   true,
   code:    500,
   message: "#{err}"}.to_json
end

error 404 do |env, err|
  {error:   true,
   code:    404,
   message: "#{err}"}.to_json
end

# The websocket entrypoint must be here because before_all
# does not apply to websocket handlers.
ws "/api/streaming" do |ws, env|
  websocket_start(ws, env, manager)
end

get "/" do
  "this is the elstat backend. you need a frontend such as https://gitlab.com/elixire/shenlong"
end

Kemal.run do
  Signal::INT.trap do
    log "shutting down"
    ctx.db.close
    Kemal.stop
    exit
  end
end
