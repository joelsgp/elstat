require "time"
require "http"
require "./alerts"

alias Embed = Hash(String, Int32 | String | Hash(String, String))

# Represents a discord webhook alert
class DiscordAlert < Alert
  @webhooks : Array(String)

  def initialize(@alert_name : String, @ctx : Context)
    @webhooks = @ctx.cfg["alert:#{@alert_name}"]["url"].split(',')
  end

  # Post an embed to a webhook.
  private def post_webhook(embed)
    @webhooks.each do |webhook_url|
      @ctx.log.info("posting to webhook...")
      resp = HTTP::Client.post(
        url: webhook_url,
        headers: HTTP::Headers{"Content-Type" => "application/json"},
        body: {
          content: "",
          embeds:  [embed],
        }.to_json
      )

      @ctx.log.info("webhook call : #{resp.status_code}")

      if !resp.success?
        @ctx.log.warn("webhook call fail : #{resp.body}")
      end
    end
  end

  def basic_status_embed(
    color : Int32, serv_name : String, result
  ) : Embed
    embed = Embed.new
    embed["color"] = color
    embed["title"] = serv_name

    # convert FullAdapterResult["timestamp"] to an ISO 8601 date.
    # problem is, result.timestamp is an unix timestamp in milliseconds,
    # so some type juggling is required

    unix_ts = result.timestamp.as(Int64)
    embed["timestamp"] = Time::Format::ISO_8601_DATE_TIME.format(
      Time.unix_ms(unix_ts))

    return embed
  end

  def basic_latency_embed(color, serv_name, result) : Embed
    embed = basic_status_embed color, serv_name, result
    embed["description"] = "current latency: #{result.latency}ms"
    return embed
  end

  def alert_err(serv_name : String, result : FullAdapterResult)
    embed = basic_status_embed 0xff0000, serv_name, result
    embed["description"] = "error: #{result.error}"
    post_webhook embed
  end

  def alert_err_ok(serv_name : String, result : FullAdapterResult)
    embed = basic_status_embed 0x00ff00, serv_name, result

    # calculate downtime
    downtime = calc_downtime serv_name, result
    embed["footer"] = {
      "text" => "down for #{downtime.total_minutes} minutes (#{downtime.total_seconds} seconds)",
    }

    post_webhook embed
  end

  def alert_slow(serv_name : String, result : FullAdapterResult)
    embed = basic_latency_embed 0xff7f50, serv_name, result
    post_webhook embed
  end

  def alert_slow_ok(serv_name : String, result : FullAdapterResult)
    embed = basic_latency_embed 0x00ff00, serv_name, result
    post_webhook embed
  end

  def basic_incident_embed(color, incident, ts)
    embed = Embed.new
    embed["color"] = color
    embed["timestamp"] = Time::Format::ISO_8601_DATE_TIME.format(ts)
    embed["footer"] = {
      "text" => "incident id: #{incident.id.to_s}",
    }
    return embed
  end

  def alert_incident_new(incident)
    embed = basic_incident_embed(
      0xff0000, incident, Time.unix_ms(incident.start_ts))

    embed["title"] =
      "New Incident - #{incident.incident_type} - #{incident.title}"
    embed["description"] = incident.content
    post_webhook embed
  end

  def alert_incident_close(incident : Incident)
    end_ts = incident.end_ts

    if end_ts.nil?
      return
    end

    embed = basic_incident_embed(
      0x00ff00, incident, Time.unix_ms(end_ts))
    embed["title"] = "Closed incident - #{incident.title}"

    post_webhook embed
  end

  def alert_incident_new_stage(incident : Incident)
    stage = incident.stages.last(1)[0]
    embed = basic_incident_embed(
      0xffff00, incident, Time.unix_ms(stage.timestamp))

    embed["title"] = "Stage for '#{incident.title}' - #{stage.title}"
    embed["description"] = stage.content

    post_webhook embed
  end
end
