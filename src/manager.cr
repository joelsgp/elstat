require "http"
require "uuid"

require "./context"
require "./adapters/*"
require "./alerts/*"
require "./worker"

require "./api/streaming"
require "./api/incidents"

require "./expiring_hash"
require "./drop_queue"

# Maps adapter strings to Adapter subclasses
ADAPTERS = {
  "ping" => PingAdapter,
  "http" => HTTPAdapter,
}

# Maps alert strings to Alert subclasses
ALERTS = {
  "discord" => DiscordAlert,
  "email"   => EMailAlert,
}

alias Service = Hash(String, String)
alias WebsocketID = UUID

enum AlertType
  Error
  ErrorOk
  Slow
  SlowOk
end

enum RowState
  Ok
  Down
  Slow
end

alias UptimeData = String
alias UptimeCache = ExpiringHash(Tuple(String, Time::Span), UptimeData)
alias MonthUptimeCache = ExpiringHash(Tuple(String, Time::MonthSpan), UptimeData)
alias GraphElement = Tuple(Int64, Int64)
alias GraphDeque = DropQueue(GraphElement)

# Main class for everything related to Elstat.
# Creates tables, manages websocket subscriptions, spawns service workers, etc.
class Manager
  @workers = {} of String => Fiber

  @subscribers = {} of String => Set(WebsocketID)
  @websockets = {} of WebsocketID => HTTP::WebSocket

  @main_channel = Channel(WorkerMessage).new
  @state = {} of String => FullAdapterResult

  @uptime_cache = UptimeCache.new(5.minutes)
  @month_uptime_cache = MonthUptimeCache.new(5.minutes)

  @graph_data = {} of String => GraphDeque

  def initialize(@context : Context)
  end

  def context
    @context
  end

  def uptime_cache
    @uptime_cache
  end

  def month_uptime_cache
    @month_uptime_cache
  end

  def service_keys : Array(String)
    root = @context.cfg["elstat"]

    res = [] of String

    root["order"].split(',').each do |service_name|
      res.push("service:#{service_name}")
    end

    return res
  end

  def service_names : Array(String)
    self.service_keys.map(&.lchop("service:")).to_a
  end

  def alert_keys
    @context.cfg.each_key.select &.starts_with?("alert:")
  end

  private def adapter_from_name(service_name : String) : Adapter.class
    serv = @context.cfg["service:#{service_name}"]
    adapter_name = serv["adapter"]
    adapter = ADAPTERS[adapter_name]
    return adapter
  end

  def make_db_table(name : String, service : Service)
    @context.db.exec "
      create table if not exists #{name} (
        timestamp bigint,
        status bool,
        latency bigint
      )
    "

    @context.db.exec "
      CREATE TABLE IF NOT EXISTS incidents (
          id text PRIMARY KEY,
          incident_type text,
          title text,
          content text,
          ongoing bool,
          start_timestamp bigint,
          end_timestamp bigint
      );"

    @context.db.exec "CREATE TABLE IF NOT EXISTS incident_stages (
          parent_id text REFERENCES incidents (id) NOT NULL,
          timestamp bigint,
          title text,
          content text,
          PRIMARY KEY (parent_id, timestamp)
      );"
  end

  private def commit_result(serv_name : String, result : AdapterResult) : Int64
    timestamp = Time.utc.to_unix_ms.to_i64
    @context.db.exec(
      "insert into #{serv_name} (timestamp, status, latency)
         values (?, ?, ?)",
      args: [timestamp, result.status, result.latency])

    # since timestamp is created here, we can always assume that adding a new
    # graph element to the graph store will always be recent
    @graph_data[serv_name].push({timestamp, result.latency})
    return timestamp
  end

  private def ws_chan_prefixes : Array(String)
    return ["status", "latency"]
  end

  # Spawns a fiber which then handles channel dispatching.
  private def dispatch_result(
    serv_name : String, timestamp : Int64, result : AdapterResult
  )
    spawn self.dispatch_channel(
      "status:#{serv_name}", timestamp, result.status)
    spawn self.dispatch_channel(
      "latency:#{serv_name}", timestamp, result.latency)
  end

  # Iterate through all subscribers of a channel and send the given
  # value through it.
  private def dispatch_channel(
    channel : String, timestamp : Int64, value : AdapterValue
  )
    @context.log.debug("dispatching to #{channel} : #{value}")

    stringified = {
      op: MessageOP::Data.to_i32,
      c: channel, d: [timestamp, value],
    }.to_json

    @subscribers[channel].each do |client_id|
      ws = @websockets[client_id]
      ws.send(stringified)
    end
  end

  # Split the given alert string into Alert instances
  private def alerts_from_str(alert_id_str : String?) : Array(Alert)
    res = [] of Alert

    unless alert_id_str.nil?
      alert_id_str.split(',').each do |alert_id|
        type_id = @context.cfg["alert:#{alert_id}"]["type"]
        res.push(ALERTS[type_id].new(alert_id, @context))
      end
    end

    return res
  end

  # Return the Alert subclass instances responsible for the service
  private def get_alerts(serv_name : String) : Array(Alert)
    cfg = context.cfg
    root_alerts = alerts_from_str(cfg["elstat"]["alerts"]?)
    service_alerts = alerts_from_str(cfg["service:#{serv_name}"]["alerts"]?)
    return root_alerts + service_alerts
  end

  private def get_row_state(service : String, row : Tuple(Int64, Bool, Int64)) : RowState
    latency_threshold = @context.thresholds(service)[:latency]
    if row[1] && row[2] > latency_threshold
      RowState::Slow
    elsif row[1]
      RowState::Ok
    else
      RowState::Down
    end
  end

  private def rows_are(rows, row_state : RowState) : Bool
    return rows.all? { |state| state == row_state }
  end

  private def rows_are_not(rows, row_state : RowState) : Bool
    return rows.all? { |state| state != row_state }
  end

  private def select_alert_type?(
    result : FullAdapterResult, serv_name : String
  ) : AlertType?
    # there can be four types of alerts
    #  - error happened
    #  - error is recovered
    #  - latency is higher than slow threshold
    #  - latency is recovered

    thresholds = @context.thresholds serv_name
    slow_threshold = thresholds[:slow]
    down_threshold = thresholds[:down]

    # the amount of rows we fetch for the alert algorithm must be
    # max(D, S) * 2 to guarantee we can see the transitions
    fetch_row_count = Math.max(slow_threshold, down_threshold) * 2

    # NOTE this will crash with an adapter that doesn't have status
    # and latency, lol!
    rows = @context.db.query_all(
      "select timestamp, status, latency
      from #{serv_name}
      order by timestamp desc
      limit #{fetch_row_count}", as: {Int64, Bool, Int64})

    # if we didn't get enough rows, our calculations will likely be wrong.
    if rows.size < fetch_row_count
      return
    end

    states = [] of RowState
    rows.each do |row|
      states.push(get_row_state(serv_name, row))
    end

    # there are 4 types of transitions, just like there are 4 types of alerts
    # not counting incidents.

    # to detect we are in a transition from state X to state Y we check
    # if all the rows from 0 to N are Y, and remaining N rows are on state X,
    # assuming row[0] is the latest row.

    # so, for example, a transition from Ok to Err (making an Err alert),
    # means rows 0 to N are Err, and remaining N rows are Ok.

    # there are 4 slices we want to know about in the array of rows:
    #  - rows going from 0 to S. and from 0 to N
    #  - and from S to end, and N to end (the remaining ones)
    down_rows = states[0..down_threshold - 1]
    slow_rows = states[0..slow_threshold - 1]
    rows_from_down = states[down_threshold..]
    rows_from_slow = states[slow_threshold..]

    # we then need to check from those slices if they're ALL from a given
    # row state (Ok, Down, Slow), that would give us 12 different combinations to check
    # but we are only interested in our 4 transitions, that leaves 8 row states to check,
    # since each transition is composed of two row states.

    # Err: down_rows is down, but rows_from_down is ok OR slow
    err_transition = rows_are(down_rows, RowState::Down) && rows_are_not(rows_from_down, RowState::Down)

    # ErrOk: down_rows is ok, but rows_from_down were down
    err_ok_transition = rows_are(down_rows, RowState::Ok) && rows_are(rows_from_down, RowState::Down)

    # same applies for Slow (Slow and Ok) and SlowOk (Ok and Slow)
    slow_transition = rows_are(slow_rows, RowState::Slow) && rows_are_not(rows_from_slow, RowState::Slow)
    slow_ok_transition = rows_are(slow_rows, RowState::Ok) && rows_are(rows_from_slow, RowState::Slow)

    @context.log.debug("states: #{states}, S=#{slow_threshold}, D=#{down_threshold}")
    @context.log.debug("down_rows=#{down_rows}, slow_rows=#{slow_rows}")
    @context.log.debug("rows_from_down=#{rows_from_down}, rows_from_slow=#{rows_from_slow}")

    # map one and only one transition we found into an alert type.
    # following the rule that Err* transitions are more important than Slow* trantitions
    if err_transition
      return AlertType::Error
    elsif err_ok_transition
      return AlertType::ErrorOk
    elsif slow_transition
      return AlertType::Slow
    elsif slow_ok_transition
      return AlertType::SlowOk
    end

    # if neither of those happened, return nil, as thre is no alert
    return
  end

  private def do_alert(
    alert : Alert, alert_type : AlertType,
    serv_name : String, result : FullAdapterResult
  )
    @context.log.warn("Sent #{serv_name}'s alert to #{alert}")

    case alert_type
    when AlertType::Error
      alert.alert_err serv_name, result
    when AlertType::ErrorOk
      alert.alert_err_ok serv_name, result
    when AlertType::Slow
      alert.alert_slow serv_name, result
    when AlertType::SlowOk
      alert.alert_slow_ok serv_name, result
    end
  end

  private def check_alerts(serv_name : String)
    result = @state[serv_name]

    service_alerts = self.get_alerts(serv_name)

    # select which type of alert to do, based on checks
    alert_type = self.select_alert_type?(result, serv_name)

    if !alert_type.nil?
      @context.log.warn("service #{serv_name} alert : #{alert_type}")

      service_alerts.each do |alert|
        spawn do_alert(alert, alert_type, serv_name, result)
      end
    end
  end

  private def handle_result(serv_name : String, result : AdapterResult)
    # regardless of the error state, we'll commit it to the database
    # for proper uptime calculations. the actual error isn't commited due
    # to the "error" field not being in the adapter's spec.db (which is good)
    timestamp = self.commit_result(serv_name, result)
    self.dispatch_result(serv_name, timestamp, result)

    @state[serv_name] = FullAdapterResult.new(timestamp, result)
    self.check_alerts(serv_name)
  end

  private def handle_notify(serv_name : String, result : NotifyMessage)
    case result.notification_type
    when NotifyType::Crash
      @context.log.error("Got crash notification. #{result.message}")

      # we resend the notification as an error so we can have alert
      # capabilities when a worker crashes.

      # the worker will still keep crashing, but due to the alert algorithms
      # only one alert will be dispatched with the error type, which is good.
      adp_result = AdapterResult.new(false, 0, result.message)
      self.handle_result(serv_name, adp_result)
    else
      @context.log.error("Got invalid notification type: #{result.notification_type}")
    end
  end

  # Waits for the service workers to send data
  # through the main channel. This fiber is the only one
  # doing a receive() on @main_channel.
  private def fiber_main
    while true
      value = @main_channel.receive
      @context.log.debug("got value: #{value}")

      serv_name = value[0]
      message_type, message = value[1]

      case message_type
      when :notify
        self.handle_notify(serv_name, message.as(NotifyMessage))
      when :result
        self.handle_result(serv_name, message.as(AdapterResult))
      else
        @context.log.warn("Got wrong message type: #{message_type}")
      end
    end
  end

  # We only create a websocket channel when the given
  # prefix is in the service adapter's spec.
  private def check(prefix : String, name : String)
    ws_chan_name = "#{prefix}:#{name}"
    @subscribers[ws_chan_name] = Set(WebsocketID).new
    @context.log.info("Created channel '#{ws_chan_name}'")
  end

  private def ws_channel_setup(service : Service, name : String)
    ws_chan_prefixes().each do |prefix|
      self.check(prefix, name)
    end
  end

  def fetch_database_graphs(service_name : String) : Tuple(Array(GraphElement), Time::Span)
    t1 = Time.monotonic
    rows = @context.db.query_all(
      "select timestamp, latency from #{service_name}
      order by timestamp desc
      limit 50", as: {Int64, Int64})
    t2 = Time.monotonic
    elapsed = t2 - t1

    return rows, elapsed
  end

  def fetch_all_graphs : Hash(String, Array(GraphElement))
    res = {} of String => Array(GraphElement)

    service_names.each do |serv_name|
      rows = @graph_data[serv_name]

      res[serv_name] = Array(GraphElement).new
      rows.deque.each { |row| res[serv_name].push(row) }
    end

    return res
  end

  # initialize the manager with its channels
  # and tables
  def setup
    @subscribers["incidents"] = Set(WebsocketID).new

    spawn fiber_main()

    # for each service we need to:
    # - Create tables for it
    # - Create websocket channels
    # - Start a service worker, giving it @main_channel
    self.service_names.each do |name|
      service = @context.cfg["service:#{name}"]

      self.make_db_table(name, service)
      self.ws_channel_setup(service, name)

      # query data for graphs to fill in the in-memory graph state
      rows, timing = fetch_database_graphs(name)
      @context.log.info("Loaded #{name} graph data in #{timing.total_milliseconds.to_u64}ms")
      @graph_data[name] = GraphDeque.new(50, rows)

      worker = ServiceWorker.new(name, service, @main_channel, @context)
      spawn worker.run

      # the worker can take any time to start up, so the
      # manager must fill in data to not crash if
      # a client requests /api/status before worker replies
      timestamp = Time.utc.to_unix_ms.to_i64
      res = FullAdapterResult.new(timestamp, false, 0.to_i64)
      @state[name] = res
    end
  end

  # Subscribe the given websocket (with its client ID) to a list of channels.
  # Registers the websocket's Client ID into the internal websocket mapping.
  # Returns the list of channels that the client successfully subscribed
  # to.
  def subscribe(ws, client_id, channels : Array(String)) : Array(String)
    @websockets[client_id] = ws
    subscribed = [] of String

    channels.each do |channel|
      subs = @subscribers[channel]?
      if !subs.nil?
        subs.add(client_id)
        subscribed.push(channel)
      end
    end

    @context.log.info("subscribed #{client_id} to #{subscribed.size} channels")
    subscribed
  end

  def subscribe_all(ws, client_id) : Array(String)
    @websockets[client_id] = ws
    subscribed = [] of String

    @subscribers.each do |channel, _subscriber_set|
      subscribed.push(channel)
    end

    @context.log.info("subscribing #{client_id} to (all) #{subscribed.size} channels")
    subscribe(ws, client_id, subscribed)
  end

  # Unsubscribe the given WebsocketID from the given list of channels.
  def unsubscribe(
    client_id : WebsocketID, channels : Array(String)
  ) : Array(String)
    unsubbed = [] of String

    channels.each do |channel|
      subs = @subscribers[channel]?

      if (!subs.nil?) && subs.delete(client_id)
        unsubbed.push(channel)
      end
    end

    @context.log.info("unsubbed #{client_id} from #{unsubbed}")
    return unsubbed
  end

  # Remove the given WebsocketID from all channels.
  def rm_websocket(client_id : WebsocketID) : Array(String)
    unsubbed = unsubscribe(client_id, @subscribers.keys)

    # We remove the client id from @websockets AFTER the unsubscribe()
    # call to not cause havoc considering concurrency.
    @websockets.delete(client_id)
    return unsubbed
  end

  def fetch_stages(incident_id : UUID) : Array(IncidentStage)
    res = [] of IncidentStage

    rows = @context.db.query_all(
      "select timestamp, title, content
      from incident_stages
      where parent_id = ?
      order by timestamp asc", incident_id.to_s, as: {Int64, String, String})

    rows.each do |row|
      res.push(IncidentStage.new(*row))
    end

    return res
  end

  # Fetch a single incident.
  def fetch_incident(incident_id : UUID) : Incident?
    incident = @context.db.query_one?(
      "select id, incident_type, title, content, ongoing, start_timestamp, end_timestamp
      from incidents
      where id = ?", incident_id.to_s, as: {String, String, String, String, Bool, Int64, Int64?})

    if incident.nil?
      return nil
    end

    incident = Incident.new(*incident)
    incident.stages = fetch_stages(incident_id)

    return incident
  end

  # Publish a given opcode
  def dispatch_incident(opcode : MessageOP, data : Incident)
    case opcode
    when MessageOP::IncidentNew,
         MessageOP::IncidentUpdate,
         MessageOP::IncidentClose
      spawn do
        stringified = {
          op: opcode.to_i32,
          c:  "incidents",
          d:  data.to_map,
        }.to_json
        @subscribers["incidents"].each do |client_id|
          ws = @websockets[client_id]
          ws.send(stringified)
        end
      end
    else
      raise "invalid op code"
    end
  end

  def alert_incident_one(inc_alert_type : IncidentAlertType, incident : Incident, alert)
    case inc_alert_type
    when IncidentAlertType::Create
      alert.alert_incident_new incident
    when IncidentAlertType::Close
      alert.alert_incident_close incident
    when IncidentAlertType::NewStage
      alert.alert_incident_new_stage incident
    end
  end

  # Make an alert for a given incident. Spawns fibers that handle the
  # incident alert, for each declared alert.
  def alert_incident(inc_alert_type : IncidentAlertType, incident_id : UUID)
    root = @context.cfg["elstat"]
    cfg_alerts = root["incident_alert"]?
    if cfg_alerts.nil?
      return
    end

    alerts = alerts_from_str(cfg_alerts)
    incident = fetch_incident incident_id
    if incident.nil?
      @context.log.warn "incident given to alert not found"
      return
    end

    # the compiler wrongly assumes incident to be Incident?
    # instead of Incident, even after a nil check that does a return,
    # because of 'spawn'

    # check https://github.com/crystal-lang/crystal/issues/3093 for
    # more details on this.

    # for now, the workaround is a copy
    _incident = incident

    alerts.each do |alert|
      spawn do
        alert_incident_one(inc_alert_type, _incident, alert)
      end
    end
  end
end
