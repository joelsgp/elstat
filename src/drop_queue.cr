class DropQueue(T)
  property deque : Deque(T)
  property max_size : UInt64

  def initialize(@max_size : UInt64)
    @deque = Deque(T).new(max_size)
  end

  def initialize(@max_size : UInt64, existing_array : Array(T))
    @deque = Deque(T).new(existing_array)
  end

  def deque
    @deque
  end

  def push(value : T)
    if @deque.size == @max_size
      @deque.shift
    end

    @deque.push value
  end
end
